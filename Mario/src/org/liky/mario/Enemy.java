package org.liky.mario;

import java.awt.image.BufferedImage;

public class Enemy implements Runnable {
	

	private int x,y;
	private int startX,startY;
	private int type;
	private BufferedImage showImage=null;

	//
	private BackGround bg;
	

	private boolean isLeftOrUp=true;

	private int upMax;
	private int downMax;
	private Thread t=new Thread(this);
	

	private int imageType=0;
	

	public Enemy(int x,int y,boolean isLeft,int type,BackGround bg){
		this.x=x;
		this.y=y;
		this.startX=x;
		this.startY=y;
		this.isLeftOrUp=isLeft;
		this.type=type;
		this.bg=bg;
		if(type==1){
			this.showImage=StaticValue.allTriangleImage.get(0);
		}
		t.start();
		t.suspend();
	}
	//ʳ�˻�
	public Enemy(int x,int y,boolean isUp,int type,int upMax,int downMax,BackGround bg){
		this.x=x;
		this.y=y;
		this.startX=x;
		this.startY=y;
		this.isLeftOrUp=isUp;
		this.type=type;
		this.upMax=upMax;
		this.downMax=downMax;
		this.bg=bg;
		if(type==2){
			this.showImage=StaticValue.allFlowerImage.get(0);
		}
		t.start();
		t.suspend();
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		while(true){
			
			
			boolean canLeft=true;
			boolean canRight=true;

			boolean onLand=false;
			for(int i=0;i<this.bg.getAllObstruction().size();i++){
				Obstruction ob=bg.getAllObstruction().get(i);

				if(ob.getX()==this.x+60&&(ob.getY()+50>this.y&&ob.getY()-50<this.y)){
					canRight=false;
				}

				if(ob.getX()==this.x-60&&(ob.getY()+50>this.y&&ob.getY()-50<this.y)){
					canLeft=false;
				}
			}
			
			if(!canLeft&&this.isLeftOrUp||this.x==0){
				this.isLeftOrUp=false;
			}else if(!canRight&&!this.isLeftOrUp||this.x==840){
				this.isLeftOrUp=true;
			}
		
			
			if(type==1){
				if(this.isLeftOrUp){
					this.x-=2;
				}else{
					this.x+=2;
				}
				if(imageType==0){
					imageType=1;
				}else{
					imageType=0;
				}
				this.showImage=StaticValue.allTriangleImage.get(imageType);
			}
			if(type==2){
				if(this.isLeftOrUp){
					this.y-=2;
				}else{
					this.y+=2;
				}
			
				if(imageType==0){
					imageType=1;
				}else{
					imageType=0;
				}
				
				if(this.isLeftOrUp&&this.y==this.upMax){
					this.isLeftOrUp=false;
				}
				if(!this.isLeftOrUp&&this.y==this.downMax){
					this.isLeftOrUp=true;
				}
				
				this.showImage=StaticValue.allFlowerImage.get(imageType);
			}
			try {
				Thread.sleep(51);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}	
	}
	
	public void dead(){
		this.showImage=StaticValue.allTriangleImage.get(2);
		this.bg.getAllEnemy().remove(this);
		this.bg.getRemovedEnemy().add(this);
	}
	
	public void startMove(){
		t.resume();
	}
	public void reset(){

		this.x=this.startX;
		this.y=this.startY;

		if(this.type==1){
			this.showImage=StaticValue.allTriangleImage.get(0);
		}else if(this.type==2){
			this.showImage=StaticValue.allFlowerImage.get(0);
		}
	}
	
	
	
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getStartX() {
		return startX;
	}
	public void setStartX(int startX) {
		this.startX = startX;
	}
	public int getStartY() {
		return startY;
	}
	public void setStartY(int startY) {
		this.startY = startY;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public BufferedImage getShowImage() {
		return showImage;
	}
	public void setShowImage(BufferedImage showImage) {
		this.showImage = showImage;
	}
	public boolean isLeftOrUp() {
		return isLeftOrUp;
	}
	public void setLeftOrUp(boolean isLeftOrUp) {
		this.isLeftOrUp = isLeftOrUp;
	}
	public int getUpMax() {
		return upMax;
	}
	public void setUpMax(int upMax) {
		this.upMax = upMax;
	}
	public int getDownMax() {
		return downMax;
	}
	public void setDownMax(int downMax) {
		this.downMax = downMax;
	}

	
}
