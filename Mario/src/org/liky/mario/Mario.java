package org.liky.mario;

import java.awt.image.BufferedImage;



public class Mario implements Runnable{



	public boolean isClear() {
		return isClear;
	}

	public void setClear(boolean isClear) {
		this.isClear = isClear;
	}


	private int x,y;
	//Speed
	private int xmove=5,ymove=5;

	private String status;

	private BufferedImage showImage;

	private int heal,score;
	
	private Thread t=null;
	

	private BackGround bg=null;
	

	private int moving=0;

	private int upTime=0;
	

	private boolean isDead=false;
	

	private boolean isClear=false;
	

	public Mario(int x,int y){
		this.x=x;
		this.y=y;

		this.showImage=StaticValue.allMarioImage.get(0);
		this.heal=3;
		this.score=0;
		this.status="right--standing";
		
		t=new Thread(this);
		t.start();
	}
	
	public void run(){
		while(true){
			

			if(this.bg.isFlag()&&this.x>=520){
				this.bg.setOver(true);
				if(this.bg.isDown()){

					this.status="right--moving";
					if(this.x<580){

						x+=5;
					}else{
						x+=5;
						if(y<480){

							y+=5;
						}

						if(x>=780){
							isClear=true;
						}
					}
					
				}else{
					if(this.y<420){
						this.y+=5;
					}
					if(this.y>=420){
						this.y=420;
						this.status="right--standing";
		
					}
				}
			}else{			
				//�ж��Ƿ�����ƶ�
				boolean canLeft=true;
				boolean canRight=true;
				//�Ƿ����ϰ�����
				boolean onLand=false;
				for(int i=0;i<this.bg.getAllObstruction().size();i++){
					Obstruction ob=bg.getAllObstruction().get(i);
					//��������
					if(ob.getX()==this.x+60&&(ob.getY()+50>this.y&&ob.getY()-50<this.y)){
						if(ob.getType()!=3){
							canRight=false;
						}
					}
					//��������
					if(ob.getX()==this.x-60&&(ob.getY()+50>this.y&&ob.getY()-50<this.y)){
						if(ob.getType()!=3){
						 canLeft=false;
						}
					}
					//��ĳ���ϰ����ϣ�������Ծ
					if(ob.getY()==this.y+60&&(ob.getX()+60>this.x&&ob.getX()-60<this.x)){
						if(ob.getType()!=3){
							onLand=true;
						}
					}
					
					
					//�ж��Ƿ񶥵��ϰ���
					if(ob.getY()==this.y-60&&(ob.getX()+50>this.x&&ob.getX()-50<this.x)){
						//��ש����д���
						if(ob.getType()==0){
							this.bg.getAllObstruction().remove(ob);
							this.bg.getRemovedObstruction().add(ob);
						}
						
						//�����ʺŵĴ���Ͷ�����ש��Ĵ���
						if(ob.getType()==4||ob.getType()==3&&upTime>0){
							ob.setType(2);
							ob.setImage();
						}
						
						
						upTime=0;
					}
					
				}
	
					
				//�Ե��˵���ײ�����ж�
				for(int i=0;i<this.bg.getAllEnemy().size();i++){
					Enemy e=this.bg.getAllEnemy().get(i);
					if(e.getX()+50 > this.x&& e.getX()-50<this.x&&
							e.getY()+50 > this.y&& e.getY()-50<this.y){
						this.dead();
					}
					if(e.getY()==this.y+60&&(e.getX()+60>this.x&&
							e.getX()-60<this.x)){
						if(e.getType()==1){
							e.dead();
							this.upTime=10;
							this.ymove=-5;
						}else if(e.getType()==2){
							this.dead();
						
						}
					}
				}
	
				
				
				
				if(onLand&&upTime==0){
					if(this.status.indexOf("left")!=-1){
						if(xmove!=0){
							this.status="left--moving";
						}else{
							this.status="left--standing";
						}
					}else{
						if(xmove!=0){
							this.status="right--moving";
						}else{
							this.status="right--standing";
						}
					}
				}else{
					//��ʾ������״̬
					if(upTime!=0){
						upTime--;
					}else{
						this.down();
					}
					y+=ymove;
				}
				
				if(canLeft&&xmove<0||(canRight&&xmove>0)){
					x+=xmove;
					if(x<0){
						x=0;
					}
				}	
			}
			//����ͼƬȡ�õ�������
			int temp=0;
			//��ǰ��ǰ��
			if(this.status.indexOf("left")!=-1){
				temp+=5;
			}
			
			//�ж����ƶ�
			if(this.status.indexOf("moving")!=-1){
				temp+=moving;
				moving++;
				if(moving==4){
					moving=0;
				}
			}
			
			if(this.status.indexOf("jumping")!=-1){
				temp+=4;
			}
			//�ı���ʾͼƬ
			this.showImage=StaticValue.allMarioImage.get(temp);

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void dead(){
		this.heal--;
		if(this.heal==0){
			isDead=true;
		}else{
			bg.reset();
			this.x=0;
			this.y=480;
		}
	}
	
	
	public void leftMove(){
		xmove=-5;
		//�����ǰ����Ծ״̬��Ӧ����ԭ��״̬
		if(this.status.indexOf("jumping")!=-1){
			this.status="left--jumping";
		}else{
			this.status="left--moving";
		}
	}
	public void rightMove(){
		xmove=5;
		if(this.status.indexOf("jumping")!=-1){
			this.status="right--jumping";
		}else{
			this.status="right--moving";
		}
	}
	public void leftStop(){
		xmove=0;
		if(this.status.indexOf("jumping")!=-1){
			this.status="left--jumping";
		}else{
			this.status="left--standing";
		}
	}
	public void rightStop(){
		xmove=0;
		if(this.status.indexOf("jumping")!=-1){
			this.status="right--jumping";
		}else{
			this.status="right--standing";
		}
	}
	public void jump(){
		if(this.status.indexOf("jumping")==-1){
			if(this.status.indexOf("left")!=-1){
				this.status="left--jumping";
			}else{
				this.status="right--jumping";
			}
			ymove=-5;
			upTime=36;
		}
	}
	
	public void down(){
		if(this.status.indexOf("left")!=-1){
			this.status="left--jumping";
		}else{
			this.status="right--jumping";
		}
		ymove=5;
	}

	
	public BackGround getBg() {
		return bg;
	}

	public void setBg(BackGround bg) {
		this.bg = bg;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}


	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BufferedImage getShowImage() {
		return showImage;
	}

	public void setShowImage(BufferedImage showImage) {
		this.showImage = showImage;
	}
	
	public int getHeal() {
		return heal;
	}
	
	public void setHeal(int heal) {
		this.heal = heal;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}
	
}
