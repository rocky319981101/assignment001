package org.liky.mario;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements KeyListener,Runnable {

	
	private List<BackGround> allBG=new ArrayList<BackGround>();
	private BackGround nowBG=null;
	private Mario mario=null;
	private Thread t=new Thread(this);
	private boolean isStart=false;
	
	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		BufferedImage image=new BufferedImage(900,600,BufferedImage.TYPE_3BYTE_BGR);
		Graphics g2=image.getGraphics();
		
		if(isStart){
			//���Ʊ���
			g2.drawImage(this.nowBG.getBgImage(),0,0,this);	
			
			g2.drawString("heal"+this.mario.getHeal(),700,60);
			//���Ƶ���
			Iterator<Enemy> iterEnemy=this.nowBG.getAllEnemy().iterator();
			while(iterEnemy.hasNext()){
				Enemy en=iterEnemy.next();
				g2.drawImage(en.getShowImage(),en.getX(),en.getY(),this);
			}
			//�����ϰ���
			Iterator<Obstruction> iter=this.nowBG.getAllObstruction().iterator();
			while(iter.hasNext()){
				Obstruction ob=iter.next();
				g2.drawImage(ob.getShowImage(),ob.getX(),ob.getY(),this);
			}
			
			//����mario
			g2.drawImage(this.mario.getShowImage(),mario.getX(),mario.getY(),this);
			
		}else{
			g2.drawImage(StaticValue.startImage,0,0,this);
		}
		
		

		
		

		
		//�ѻ���ͼƬ���Ƶ�������
		g.drawImage(image,0,0,this);
		
	}
	


	public MyFrame(){
		this.setTitle("Super Mario");
		this.setSize(1000,700);
		//�õ���Ļ��С�����ÿ�ʼ����λ��
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((width-900)/2,(height-600)/2);
		//���ɸı���Ļ��С
		this.setResizable(false);
		this.addKeyListener(this);
		
		
		
		//��ʼ��ͼƬ
		StaticValue.init();
		
		//����ȫ������
		for(int i=1;i<=3;i++){
			this.allBG.add(new BackGround(i,i==3?true:false));
		}
		//��1����������Ϊ��ǰ����
		this.nowBG=this.allBG.get(0);
		
		mario=new Mario(0,600-2*60);	
		this.mario.setBg(nowBG);
		
		t.start();
		this.repaint();
		
		
		//�رպ����
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		
	}
	
	public void run(){
		while(true){
			this.repaint();
			try {
				Thread.sleep(50);
				if(this.mario.getX()>=850){
					this.nowBG=this.allBG.get(this.nowBG.getSort());
					this.mario.setBg(this.nowBG);
					this.nowBG.enemyStartMove();
					this.mario.setX(0);
				}
				
				if(this.mario.isDead()){
					JOptionPane.showMessageDialog(this,"This Game is over");
					System.exit(0);
				}
				if(this.mario.isClear()){
					JOptionPane.showMessageDialog(this,"Congratulations! You win");
					System.exit(0);
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args){
		new MyFrame();
	}
	
	
	@Override
	//����������ϵļ�ʱ 
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
//		//���ؼ����ϵ�ֵ
//		System.out.println(e.getKeyChar());
//		//���ؼ������ַ��ı���ֵ ��37 ��39 �ո�32
//		System.out.println(e.getKeyCode());
		//������39ʱ�����ƶ�->
		if(isStart){
			if(e.getKeyCode()==68){
				this.mario.rightMove();
			}
			//������<-ʱ�����ƶ�
			if(e.getKeyCode()==65){
				this.mario.leftMove();
			}
			//�����¿ո�ʱ
			if(e.getKeyCode()==87){
				this.mario.jump();
			}
		}else{
			if(e.getKeyCode()==87){
				isStart=true;
				this.nowBG.enemyStartMove();
			}
		}
		
	}

	@Override
	//��̧���ʱ
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
		//��̧��39ʱ����ֹͣ->
		if(e.getKeyCode()==68){
			this.mario.rightStop();
		}
		//��̧��<-ʱ����ֹͣ
		if(e.getKeyCode()==65){
			this.mario.leftStop();
			this.mario.setHeal(3);
			this.mario.setScore(0);
		}
	}

	@Override
	//����һЩ��Ϣʱ
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
